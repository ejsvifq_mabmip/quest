## Interface: 11302
## Author: cqwrteur
## Title: Quest
## Version: @project-version@

#@no-lib-strip@
Libs\LibStub\LibStub.lua
Libs\CallbackHandler-1.0\CallbackHandler-1.0.xml
Libs\AceAddon-3.0\AceAddon-3.0.xml
Libs\AceEvent-3.0\AceEvent-3.0.xml
Libs\AceHook-3.0\AceHook-3.0.xml
#@end-no-lib-strip@

Init.lua